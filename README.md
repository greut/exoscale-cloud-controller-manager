# Exoscale Cloud Controller Manager (CCM)

This repository provides tools and scripts for building and testing `Kubernetes cloud-controller-manager` for Exoscale. The project is under active development.

## Configuration

We have to enable the external cloudprovider, `/etc/default/kubelet`.

```ini
KUBELET_EXTRA_ARGS=--cloudprovider=external
```

## Running...

```console
% sudo go run cmd/exoscale-cloud-controller-manager/main.go \
	--kubeconfig ~/.kube/config \
	--allow-untagged-cloud
```
