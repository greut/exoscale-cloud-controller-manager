package exoscale

import (
	"context"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"time"

	"github.com/vishvananda/netlink"
)

// metadataServer has been borrowed from exoscale/exoip/metadata.go
func metadataServer() (string, error) {
	links, err := netlink.LinkList()
	if err != nil {
		return "", err
	}

	for _, link := range links {
		if link.Attrs().EncapType != "ether" {
			continue
		}

		routes, err := netlink.RouteList(link, netlink.FAMILY_V4)
		if err != nil {
			return "", err
		}

		for _, route := range routes {
			// the default route has no destination
			if route.Dst == nil && route.Gw != nil {
				return route.Gw.String(), nil
			}
		}
	}

	return "", errors.New("could not find the metadata server")
}

func FetchMetadata(ctx context.Context, name string) (string, error) {
	server, err := metadataServer()
	if err != nil {
		return "", err
	}

	url := fmt.Sprintf("http://%s/%s", server, name)
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return "", err
	}

	client := &http.Client{
		Timeout: 10 * time.Second,
	}
	resp, err := client.Do(req.WithContext(ctx))
	if err != nil {
		return "", err
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", err
	}

	return string(body), nil
}
