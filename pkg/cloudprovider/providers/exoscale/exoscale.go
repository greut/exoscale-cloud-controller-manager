package exoscale

import (
	"io"
	"io/ioutil"
	"os"

	"github.com/exoscale/egoscale"
	"k8s.io/klog"
	cloudprovider "k8s.io/kubernetes/pkg/cloudprovider"
	cloudcontroller "k8s.io/kubernetes/pkg/controller"
	"sigs.k8s.io/yaml"
)

func init() {
	cloudprovider.RegisterCloudProvider(CloudProviderName, NewCloud)
}

const (
	// CloudProviderName is the name of the exoscale european cloud provider
	CloudProviderName = "exoscale"

	defaultEndpoint = "https://api.exoscale.com/compute"
)

type Config struct {
	Endpoint  string `yaml:"endpoint,omitempty"`
	APIKey    string `yaml:"apikey"`
	SecretKey string `yaml:"secretkey"`
}

type Cloud struct {
	Config Config
	cs     *egoscale.Client
}

func NewCloud(configReader io.Reader) (cloudprovider.Interface, error) {
	config, err := parseConfig(configReader)
	if err != nil {
		return nil, err
	}

	if config.Endpoint == "" {
		klog.V(2).Infof("No API Endpoint, using %q", defaultEndpoint)
		config.Endpoint = defaultEndpoint
	}

	if config.APIKey == "" {
		klog.V(1).Info("Reading API Key from the env")
		config.APIKey = os.Getenv("EXOSCALE_API_KEY")
	}
	if config.SecretKey == "" {
		klog.V(1).Info("Reading Secret Key from the env")
		config.SecretKey = os.Getenv("EXOSCALE_SECRET_KEY")
	}

	cloud := &Cloud{
		Config: *config,
		cs:     egoscale.NewClient(config.Endpoint, config.APIKey, config.SecretKey),
	}

	return cloud, nil
}

// Initialize provides the cloud with a kubernetes client builder and may spawn goroutines
// to perform housekeeping or run custom controllers specific to the cloud provider.
// Any tasks started here should be cleaned up when the stop channel closes.
func (Cloud) Initialize(clientBuilder cloudcontroller.ControllerClientBuilder) {
	// pass
}

// LoadBalancer returns a balancer interface. Also returns true if the interface is supported, false otherwise.
func (Cloud) LoadBalancer() (cloudprovider.LoadBalancer, bool) {
	return nil, false
}

// Instances returns an instances interface. Also returns true if the interface is supported, false otherwise.
func (c *Cloud) Instances() (cloudprovider.Instances, bool) {
	return c, true
}

// Zones returns a zones interface. Also returns true if the interface is supported, false otherwise.
func (c *Cloud) Zones() (cloudprovider.Zones, bool) {
	return c, true
}

// Clusters returns a clusters interface.  Also returns true if the interface is supported, false otherwise.
func (Cloud) Clusters() (cloudprovider.Clusters, bool) {
	return nil, false
}

// Routes returns a routes interface along with whether the interface is supported.
func (Cloud) Routes() (cloudprovider.Routes, bool) {
	return nil, false
}

// ProviderName returns the cloud provider ID.
func (Cloud) ProviderName() string {
	return CloudProviderName
}

// HasClusterID returns true if a ClusterID is required and set
func (*Cloud) HasClusterID() bool {
	return false
}

func parseConfig(configReader io.Reader) (*Config, error) {
	config := new(Config)
	if configReader == nil {
		return config, nil
	}

	contents, err := ioutil.ReadAll(configReader)
	if err != nil {
		return nil, err
	}

	err = yaml.Unmarshal(contents, config)
	return config, err
}
