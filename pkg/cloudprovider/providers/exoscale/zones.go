package exoscale

import (
	"context"

	"github.com/exoscale/egoscale"
	"k8s.io/apimachinery/pkg/types"
	"k8s.io/kubernetes/pkg/cloudprovider"
)

// GetZone returns the Zone containing the current failure zone and locality region that the program is running in
// In most cases, this method is called from the kubelet querying a local metadata service to acquire its zone.
// For the case of external cloud providers, use GetZoneByProviderID or GetZoneByNodeName since GetZone
// can no longer be called from the kubelets.
func (c *Cloud) GetZone(ctx context.Context) (cloudprovider.Zone, error) {
	zone := cloudprovider.Zone{}

	zoneName, err := FetchMetadata(ctx, "latest/availability-zone")
	if err != nil {
		return zone, err
	}

	zone.Region = zoneName

	return zone, nil
}

// GetZoneByProviderID returns the Zone containing the current zone and locality region of the node specified by providerID
// This method is particularly used in the context of external cloud providers where node initialization must be done
// outside the kubelets.
func (c *Cloud) GetZoneByProviderID(ctx context.Context, providerID string) (cloudprovider.Zone, error) {
	zone := cloudprovider.Zone{}

	id, err := egoscale.ParseUUID(providerID)
	if err != nil {
		return zone, cloudprovider.InstanceNotFound
	}

	vms, err := c.cs.ListWithContext(ctx, &egoscale.VirtualMachine{
		ID: id,
	})
	if err != nil {
		return zone, cloudprovider.InstanceNotFound
	}

	vm := vms[0].(*egoscale.VirtualMachine)
	zone.Region = vm.ZoneName

	return zone, nil
}

// GetZoneByNodeName returns the Zone containing the current zone and locality region of the node specified by node name
// This method is particularly used in the context of external cloud providers where node initialization must be done
// outside the kubelets.
func (c *Cloud) GetZoneByNodeName(ctx context.Context, nodeName types.NodeName) (cloudprovider.Zone, error) {
	zone := cloudprovider.Zone{}

	vms, err := c.cs.ListWithContext(ctx, &egoscale.VirtualMachine{
		Name: string(nodeName),
	})
	if err != nil {
		return zone, err
	}

	if len(vms) != 1 {
		return zone, cloudprovider.InstanceNotFound
	}

	vm := vms[0].(*egoscale.VirtualMachine)
	zone.Region = vm.ZoneName

	return zone, nil
}
