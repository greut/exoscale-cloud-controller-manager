package exoscale

import (
	"context"

	"github.com/exoscale/egoscale"
	"k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/types"
	"k8s.io/kubernetes/pkg/cloudprovider"
)

// NodeAddresses returns the addresses of the specified instance.
// TODO(roberthbailey): This currently is only used in such a way that it
// returns the address of the calling instance. We should do a rename to
// make this clearer.
func (c *Cloud) NodeAddresses(ctx context.Context, name types.NodeName) ([]v1.NodeAddress, error) {
	vms, err := c.cs.ListWithContext(ctx, &egoscale.VirtualMachine{
		Name: string(name),
	})
	if err != nil {
		return nil, cloudprovider.InstanceNotFound
	}

	return nodeAddresses(vms)
}

// NodeAddressesByProviderID returns the addresses of the specified instance.
// The instance is specified using the providerID of the node. The
// ProviderID is a unique identifier of the node. This will not be called
// from the node whose nodeaddresses are being queried. i.e. local metadata
// services cannot be used in this method to obtain nodeaddresses
func (c *Cloud) NodeAddressesByProviderID(ctx context.Context, providerID string) ([]v1.NodeAddress, error) {
	id, err := egoscale.ParseUUID(providerID)
	if err != nil {
		return nil, cloudprovider.InstanceNotFound
	}

	vms, err := c.cs.ListWithContext(ctx, &egoscale.VirtualMachine{
		ID: id,
	})
	if err != nil {
		return nil, cloudprovider.InstanceNotFound
	}

	return nodeAddresses(vms)
}

func nodeAddresses(vms []interface{}) ([]v1.NodeAddress, error) {
	if len(vms) != 1 {
		return nil, cloudprovider.InstanceNotFound
	}

	addresses := []v1.NodeAddress{{
		Type:    v1.NodeExternalIP,
		Address: vms[0].(*egoscale.VirtualMachine).IP().String(),
	}}

	return addresses, nil
}

// InstanceID returns the cloud provider ID of the node with the specified NodeName.
// Note that if the instance does not exist, we must return ("", cloudprovider.InstanceNotFound)
// cloudprovider.InstanceNotFound should NOT be returned for instances that exist but are stopped/sleeping
func (c *Cloud) InstanceID(ctx context.Context, nodeName types.NodeName) (string, error) {
	value, err := FetchMetadata(ctx, "latest/instance-id")
	if value == "" || err != nil {
		return "", cloudprovider.InstanceNotFound
	}
	return value, nil
}

// InstanceType returns the type of the specified instance.
func (c *Cloud) InstanceType(ctx context.Context, name types.NodeName) (string, error) {
	return "", nil
}

// InstanceTypeByProviderID returns the type of the specified instance.
func (c *Cloud) InstanceTypeByProviderID(ctx context.Context, providerID string) (string, error) {
	return "", nil
}

// AddSSHKeyToAllInstances adds an SSH public key as a legal identity for all instances
// expected format for the key is standard ssh-keygen format: <protocol> <blob>
func (c *Cloud) AddSSHKeyToAllInstances(ctx context.Context, user string, keyData []byte) error {
	return cloudprovider.NotImplemented
}

// CurrentNodeName returns the name of the node we are currently running on
// On most clouds (e.g. GCE) this is the hostname, so we provide the hostname
func (c *Cloud) CurrentNodeName(ctx context.Context, hostname string) (types.NodeName, error) {
	return types.NodeName(hostname), nil
}

// InstanceExistsByProviderID returns true if the instance for the given provider exists.
// If false is returned with no error, the instance will be immediately deleted by the cloud controller manager.
// This method should still return true for instances that exist but are stopped/sleeping.
func (c *Cloud) InstanceExistsByProviderID(ctx context.Context, providerID string) (bool, error) {
	id, err := egoscale.ParseUUID(providerID)
	if err != nil {
		return false, cloudprovider.InstanceNotFound
	}

	vms, err := c.cs.ListWithContext(ctx, &egoscale.VirtualMachine{
		ID: id,
	})
	if err != nil {
		return false, err
	}

	return len(vms) == 1, nil
}

// InstanceShutdownByProviderID returns true if the instance is shutdown in cloudprovider
func (c *Cloud) InstanceShutdownByProviderID(ctx context.Context, providerID string) (bool, error) {
	return false, cloudprovider.NotImplemented
}
