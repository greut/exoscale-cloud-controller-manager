package main

import (
	"flag"
	"fmt"
	"os"

	"github.com/exoscale/exoscale-cloud-controller-manager/pkg/cloudprovider/providers/exoscale"
	"github.com/spf13/pflag"
	utilflag "k8s.io/apiserver/pkg/util/flag"
	"k8s.io/apiserver/pkg/util/logs"
	"k8s.io/klog"
	"k8s.io/kubernetes/cmd/cloud-controller-manager/app"
)

const version = "0.0.1"

func main() {
	command := app.NewCloudControllerManagerCommand()
	pflag.CommandLine.SetNormalizeFunc(utilflag.WordSepNormalizeFunc)
	pflag.CommandLine.AddGoFlagSet(flag.CommandLine)

	logs.InitLogs()
	defer logs.FlushLogs()

	klog.V(1).Infof("exoscale-cloud-controller-manager version: %s", version)

	command.Flags().VisitAll(func(flag *pflag.Flag) {
		if flag.Name == "cloud-provider" {
			flag.Value.Set(exoscale.CloudProviderName)
			flag.DefValue = exoscale.CloudProviderName
		}
	})

	if err := command.Execute(); err != nil {
		fmt.Fprintf(os.Stderr, "error: %v\n", err)
		os.Exit(1)
	}
}
